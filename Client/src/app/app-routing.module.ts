import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsFeedComponent } from './components/newsfeed/newsfeed.component';

const routes: Routes = [
  {path:'', redirectTo: '/', pathMatch: 'full'},
  {
    path: '',
    children: [{
      path: '',
      component: NewsFeedComponent
    }]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
