import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators'
import {Router} from '@angular/router'

import { NewsService } from '../../services/news.service';
import { INewsList } from '../../models/newsModel'

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css']
})
export class NewsFeedComponent implements OnInit {

  news = [
    { id: 1, name: 'n1', author: 'a1', created_at: 'ca1' },
    { id: 2, name: 'n2', author: 'a2', created_at: 'ca2' },
    { id: 3, name: 'n3', author: 'a3', created_at: 'ca3' }
  ]

  newsList: Array<INewsList> = [];
  INewsList: INewsList;

  constructor(
    private newsService: NewsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fetch();
  }

  getRowData(event:any) {
    
    this.newsService.hiddenNews(event._id)
    .subscribe(res => {
      
      console.log(res);
      if(res){
        location.reload();
      }
    });
    console.log(event._id);
    console.log('click');
  } 

  fetch() {

    this.newsService.getNews()
      .pipe(
        map(data => {

          console.log('data news.service');
          console.log(data);

          for (const key in data)
            if (data.hasOwnProperty(key))
              this.newsList.push({ ...data[key], id: key });
          return this.newsList;
        })
      )
      .subscribe((data) => {

        console.log('data');
        console.log(data);
      })
  }

}
