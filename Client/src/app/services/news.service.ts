import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs';
import axios from 'axios';
import {INewsList} from '../models/newsModel';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  API_URI = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getNews(): Observable <INewsList> {

    return this.http.get<INewsList>(`${this.API_URI}`);

  }
  hiddenNews(id: string) {

    return this.http.patch(`${this.API_URI}/hide/${id}`,id);
  }
}
