import {Request, Response, NextFunction} from 'express';

import {storyServices} from '../Services/storyService';

class StoryController {

  public async getStories (req: Request, res: Response, next: NextFunction): Promise<any>{
    let stories;
    try {
      stories = await storyServices.getStories();
      console.log(req);
    } catch (error) {
      return next(error)
    }
    res.status(200).json(stories);
  }

  public async hideStories (req: Request, res: Response, next: NextFunction): Promise <any> {
    const sid = req.params.id;
    let hiddenStory;
    try {
      hiddenStory = await storyServices.hideStory(sid);
    } catch (error) {
      return next(error);
    }
    res.status(200).json(hiddenStory);
  }

}

export const storyController = new StoryController();