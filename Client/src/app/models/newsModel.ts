export interface INewsList {
    _id?: string,
    hidden?: boolean,
    objectID?: string,
    created_at?: Date,
    story_title?: string,
    title?: string,
    story_url?: string,
    url?: string,
    author?: string,
    _v?:number
}