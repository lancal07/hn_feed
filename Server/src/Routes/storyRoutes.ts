import {Router} from 'express';

import {storyController} from '../Controllers/storyController';

class StoryRoutes {

    public router: Router = Router();

    constructor() {

        this.config();

    }

    config(): void{

        this.router.get('/',storyController.getStories);
        this.router.patch('/hide/:id',storyController.hideStories);
    }

}

const storyRoutes = new StoryRoutes();
export default storyRoutes.router;