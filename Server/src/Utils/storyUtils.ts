import cron from 'node-cron';

import {storyServices} from '../Services/storyService';

class StoryUtils {

    constructor() {
        this.requestNewsScheduler();
    }

    public requestNewsScheduler(){
        try {
          this.requestNewsJob().then()
        } catch (error) {
          console.error(error);
        }
        try {
          cron.schedule('0 * * * *', () => {
            this.requestNewsJob().then();
          });
        } catch (error) {
          console.error(error)
        }
      }
      
    private async requestNewsJob(): Promise<any>{
  
        const stories = await storyServices.filterStories(await storyServices.fetchStories());
        for (let story of stories) {
          try {
            await storyServices.createStory(story);
          } catch (error) {
            console.error(error)
          }
        }
      }
}

export const storyUtil = new StoryUtils();