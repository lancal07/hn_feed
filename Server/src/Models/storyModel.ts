import mongoose from 'mongoose'

export interface StoryDocument extends mongoose.Document {
  objectID: String
  created_at: Date
  story_title: String
  title: String
  story_url: String
  url: String
  author: String
  hidden: Boolean,
}

const StorySchema = new mongoose.Schema({
  objectID: { type: String, required: true, unique: true},
  created_at: { type: Date, required: true},
  story_title: { type: String, required: false},
  title: { type: String, required: false},
  story_url: { type: String, required: false},
  url: { type: String, required: false},
  author: { type: String, required: true},
  hidden: { type: Boolean, required: true, default: false}
})

export default mongoose.model<StoryDocument>('Story',StorySchema);