import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {NewsFeedComponent} from '../components/newsfeed/newsfeed.component';

@NgModule({
    imports: [
      CommonModule
      
    ],
    exports: [
      CommonModule,
      NewsFeedComponent
    ],
    declarations: [NewsFeedComponent]
  
  })
  export class ComponentsModule { }