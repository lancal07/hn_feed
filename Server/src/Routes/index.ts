import {Router} from 'express';

import storyRoutes from './storyRoutes';

class IndexRoutes{

    public router: Router = Router();

    constructor() {

        this.config();
    }

    config (): void {

        // this.router.get('/',indexController.index);
        this.router.use(storyRoutes);        

    }
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;