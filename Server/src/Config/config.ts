const config = {
  db: {
    mongoUri: process.env.MONGO_URL || 'mongodb://mongo:27017/hn_feed'
  },
  server: {
    port: process.env.SERVER_PORT || 3000
  },
  api: {
    apiUri: process.env.API_URL || 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs'
  }

}

export default config