import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors';
import dotenv from 'dotenv';
import routes from './Routes/index';

import {storyUtil} from './Utils/storyUtils';

class Server {

    public app: Application;

    // constructor
    constructor() {

        this.app = express();
        this.config();
        this.middleware();
        this.routes();
        this.errorSetup();
    }

    // Middlewares
    middleware() {
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
    }

    // settings
    config(): void {

        dotenv.config()
        this.app.set('port',process.env.PORT || 3000);        
        this.app.use(cors());
    }

    // Routes
    routes(): void {

        this.app.use('/',routes);

    }

    private static handlerFatalError(error: any) :void {
        console.log(`'[Fatal Error] ' ${error && error.message}`);
        console.log(`'[Fatal Error] ' ${error && error.stack}`); 
        process.exit(1);
    }

    private errorSetup(): void{

        process.on('uncaughtException', Server.handlerFatalError);
        process.on('unhandledRejection', Server.handlerFatalError);

        this.app.use((error: any, res: express.Response, next: express.NextFunction ) => {
            if(res.headersSent){
                return next(error)
            }
            res.status(error.code || 500);
            res.json({message: error.message || 'An unknown error occured!'});
        })
    }

    // Starting Server
    async start(): Promise <void> {

        this.app.listen(this.app.get('port'), () => {
             console.log('Server on Port ', this.app.get('port'));
         });

        storyUtil.requestNewsScheduler();
    }

}

export default Server;
