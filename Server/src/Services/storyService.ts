import Story, {StoryDocument} from '../Models/storyModel';
import axios from 'axios';
import config from '../Config/config';

class StoryServices {

  private readonly apiURL: string;

  constructor() {
    this.apiURL = config.api.apiUri;
  }

  async getStories() {
    return await Story.find({hidden: false,}).sort('-created_at').exec();
  }

  async hideStory(sid: string) {
    const story = await Story.findById(sid);
    if (story) {
      story.hidden = true;
      await story.save();
    }
    return story;
  }

  async createStory(story: StoryDocument) {
    const {
      objectID, created_at, story_title,
      title, story_url, url, author
    } = story;
    if (!(await Story.findOne({objectID: objectID}).exec())) {
      const createdStory = new Story({
        objectID,
        created_at,
        story_title,
        title,
        story_url,
        url,
        author,
      });
      return await createdStory.save();
    }
    return null;
  }

  fetchStories = async () => {
    const result = await axios.get(this.apiURL);
    return result.data.hits;
  }

  filterStories = async (stories: StoryDocument[]) => {
    return stories
      .filter(story => story.story_title || story.title)
      .filter(story => story.story_url || story.url)
  }

}

export const storyServices = new StoryServices();